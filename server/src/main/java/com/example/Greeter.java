package com.example;

/**
 * This is a class.
 */
public class Greeter {

  /**
   * This is a constructor.
   */
  public Greeter() {
// Do nothing because of X and Y.
  }


  public String greet(String someone) {
    return String.format("Hello, %s!", someone);
  }
}
